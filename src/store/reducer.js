/*eslint-disable no-eval */
const initialState = {
    formula: '',
    result: '',
    values: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9],
    symbols: ['+', '-', '*', '/', '.', '(', ')']
};

const reducer = (state = initialState, action) => {
    if (action.type === 'ADD') {
        if (state.result !== '') {
            state.formula = '';
            state.result = '';
        }
        if (state.symbols.indexOf(state.formula[state.formula.length - 1]) >= 0 && !parseInt(action.value, 10)) {
            return {
                ...state,
                formula: state.formula.substring(0, state.formula.length - 1) + action.value
            }
        }
        return {
            ...state,
            formula: state.formula + action.value
        }
    }

    if (action.type === 'REMOVE') {
        return {
            ...state,
            formula: state.formula.substring(0, state.formula.length - 1)
        }
    }

    if (action.type === 'EVAL') {
        try {
            return {
                ...state,
                result: eval(state.formula)
            }
        } catch (error) {
            alert("Something went wrong. Please check your formula");
        }
    }

    if (action.type === 'CLEAR') {
        return {
            ...state,
            formula: '',
            result: ''
        }
    }

    return state;
};

export default reducer;