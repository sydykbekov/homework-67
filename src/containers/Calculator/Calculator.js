import React, {Component} from 'react';
import {connect} from 'react-redux';
import './Calculator.css';

class Calculator extends Component {
    render() {
        return (
            <div className="container">
                <h3>CALCULATOR</h3>
                <div className="formula">{this.props.formula}</div>
                <div className="result">{this.props.result}</div>
                {this.props.values.map(value => <button onClick={this.props.add} key={value}>{value}</button>)}
                {this.props.symbols.map(symbol => <button onClick={this.props.add} key={symbol}>{symbol}</button>)}
                <button style={{color: 'cyan'}} onClick={this.props.remove}>{'<'}</button>
                <button style={{color: 'cyan'}} onClick={this.props.clear}>C</button>
                <button style={{color: 'cyan'}} onClick={this.props.evaluate}>=</button>
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        formula: state.formula,
        result: state.result,
        values: state.values,
        symbols: state.symbols
    }
};

const mapDispatchToProps = dispatch => {
    return {
        add: event => dispatch({type: 'ADD', value: event.target.innerHTML}),
        remove: () => dispatch({type: 'REMOVE'}),
        evaluate: () => dispatch({type: 'EVAL'}),
        clear: () => dispatch({type: 'CLEAR'})
    }
};

export default connect(mapStateToProps, mapDispatchToProps)(Calculator);